$(document).ready(function () {
    /*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
    var gSections = $('section[id]');
    var gSize, gLoaiPizza, gDiscount, gOrder;

    /*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */
    onPageLoading();
    // Thêm sự kiện scroll cho document
    $(document).on('scroll', scrollActive);
    // Gán sự kiện click cho nút chọn size
    $(document).on('click', '.btn-chon-size', onBtnChonSizeClick);
    // Gán sự kiện click cho nút chọn loại pizza
    $(document).on('click', '.btn-chon-loai-pizza', onBtnChonLoaiPizzaClick);
    // Gán sự kiện click cho nút gửi đơn
    $('#btn-gui-don').on('click', onBtnGuiDonClick);
    // Gán sự kiện click cho nút tạo đơn
    $('#btn-tao-don').on('click', onBtnTaoDonClick);

    /*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
    // Hàm khi load trang
    function onPageLoading() {
        // Load danh sách nước uống
        getDrinkList();
    }

    // Sự kiện active menu khi đến section
    function scrollActive() {
        var vScrollY = window.pageYOffset;
        gSections.each(function () {
            var bSectionHeight = $(this).outerHeight();
            var bSectionTop = $(this).offset().top - 200;
            var bSectionId = $(this).attr('id');
            var bSection = $('.nav-link[href*=' + bSectionId + ']');

            if (vScrollY > bSectionTop && vScrollY <= bSectionTop + bSectionHeight) {
                bSection.addClass('active');
            }
            else {
                bSection.removeClass('active');
            }
        })
    }

    // Hàm khi bấm nút chọn size
    function onBtnChonSizeClick() {
        // Lấy size được chọn
        gSize = $(this).data('size');
        console.log('Size: ', gSize);
        // Đổi màu nút
        $('.btn-chon-size').removeClass('btn-primary').addClass('bg-orange').text('Chọn');
        $(this).removeClass('bg-orange').addClass('btn-primary').text('Size ' + gSize);
    }

    // Hàm khi bấm nút chọn size
    function onBtnChonLoaiPizzaClick() {
        // Lấy loại pizza được chọn
        gLoaiPizza = $(this).data('type');
        console.log('Loại pizza: ', gLoaiPizza);
        // Đổi màu nút
        $('.btn-chon-loai-pizza').removeClass('btn-primary').addClass('bg-orange').text('Chọn');
        $(this).removeClass('bg-orange').addClass('btn-primary').text(gLoaiPizza);
    }

    // Hàm khi bấm nút gửi đơn
    function onBtnGuiDonClick() {
        // Thu thập dữ liệu
        gOrder = getOderData();
        // console.log(gOrder);
        // Kiểm tra dữ liệu
        var vValid = validateOrderData(gOrder);
        if (vValid) {
            // Hiển thị chi tiết đơn hàng
            handlePreviewOrder(gOrder);
        }
    }

    // Hàm khi bấm nút tạo đơn
    function onBtnTaoDonClick() {
        $('#preview-order-modal').modal('hide');
        createOrder();
    }
    /*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình */
    // Hàm lấy danh sách đồ uống
    function getDrinkList() {
        $.ajax({
            url: "http://203.171.20.210:8080/devcamp-pizza365/drinks",
            type: "GET",
            dataType: 'json',
            success: handleGetDrinkListSuccess,
            error: handleError
        });
    }

    // Hàm khi lấy danh sách đồ uống thành công
    function handleGetDrinkListSuccess(paramResponseData) {
        paramResponseData.forEach(bDrink => {
            $('#select-drink').append($('<option>', {
                value: bDrink.maNuocUong,
                text: bDrink.tenNuocUong
            }));
        })
    }

    // Hàm khi gọi api thất bại
    function handleError(paramError) {
        console.assert(paramError.responseText);
    }

    // Hàm lấy thông tin order
    function getOderData() {
        var vFullname = $('#inp-fullname').val().trim();
        var vEmail = $('#inp-email').val().trim();
        var vSoDienThoai = $('#inp-so-dien-thoai').val().trim();
        var vDiaChi = $('#inp-dia-chi').val().trim();
        var vVoucherId = $('#inp-voucher-id').val().trim();
        var vMessage = $('#inp-message').val().trim();
        var vNuocUong = $('#select-drink').val().trim();
        // Get size
        var vDuongKinh, vSuon, vSalad, vSoLuongNuoc, vThanhTien;
        switch (gSize) {
            case 'S':
                vDuongKinh = '20';
                vSuon = '2';
                vSalad = '200';
                vSoLuongNuoc = '2';
                vThanhTien = '150000';
                break;

            case 'M':
                vDuongKinh = '25';
                vSuon = '4';
                vSalad = '300';
                vSoLuongNuoc = '3';
                vThanhTien = '200000';
                break;

            case 'L':
                vDuongKinh = '30';
                vSuon = '8';
                vSalad = '500';
                vSoLuongNuoc = '4';
                vThanhTien = '250000';
                break;

            default:
                break;
        }
        return {
            kichCo: gSize,
            duongKinh: vDuongKinh,
            suon: vSuon,
            salad: vSalad,
            loaiPizza: gLoaiPizza,
            idVourcher: vVoucherId,
            idLoaiNuocUong: vNuocUong,
            soLuongNuoc: vSoLuongNuoc,
            hoTen: vFullname,
            thanhTien: vThanhTien,
            email: vEmail,
            soDienThoai: vSoDienThoai,
            diaChi: vDiaChi,
            loiNhan: vMessage
        }
    }

    // Hàm kiểm tra order
    function validateOrderData(paramOrderData) {
        var vResult = true;
        if (!paramOrderData.kichCo) {
            alert('Bạn chưa chọn size');
            vResult = false;
        }
        else if (!paramOrderData.loaiPizza) {
            alert('Bạn chưa chọn loại pizza');
            vResult = false;
        }
        else if (!paramOrderData.idLoaiNuocUong) {
            alert('Bạn chưa chọn nước uống');
            vResult = false;
        }
        else if (!paramOrderData.hoTen) {
            alert('Bạn chưa nhập tên');
            vResult = false;
        }
        else if (!isEmail(paramOrderData.email)) {
            alert('Bạn chưa nhập email hoặc email chưa đúng định dạng');
            vResult = false;
        }
        else if (isNaN(paramOrderData.soDienThoai) || !paramOrderData.soDienThoai) {
            alert('Bạn chưa nhập số điện thoại hoặc số điện thoại chưa đúng');
            vResult = false;
        }
        else if (!paramOrderData.diaChi) {
            alert('Bạn chưa nhập địa chỉ');
            vResult = false;
        }
        return vResult;
    }

    // Hàm kiểm tra email
    function isEmail(paramEmail) {
        var vViTriA = paramEmail.indexOf('@');
        if (vViTriA <= 0 || vViTriA == paramEmail.length - 1 || !paramEmail.includes('.')) {
            return false;
        }
        return true;
    }

    // Hàm hiển thị thông tin đơn hàng
    function handlePreviewOrder(paramOrderData) {
        $('#inp-prev-fullname').val(paramOrderData.hoTen);
        $('#inp-prev-so-dien-thoai').val(paramOrderData.soDienThoai);
        $('#inp-prev-dia-chi').val(paramOrderData.diaChi);
        $('#inp-prev-loi-nhan').val(paramOrderData.loiNhan);
        $('#inp-prev-ma-giam-gia').val(paramOrderData.idVourcher);
        // Kiểm tra voucher
        paramOrderData.idVourcher && getOrderDiscount(paramOrderData.idVourcher);
        $('#text-prev-thong-tin-chi-tiet').val(
            `Xác nhận: ${paramOrderData.hoTen}, ${paramOrderData.soDienThoai}, ${paramOrderData.diaChi}.\n` +
            `Menu ${paramOrderData.kichCo}, sườn nướng ${paramOrderData.suon}, nước ${paramOrderData.soLuongNuoc},...\n` +
            `Loại pizza: ${paramOrderData.loaiPizza.toLowerCase()}, Giá: ${paramOrderData.thanhTien}${paramOrderData.idVourcher && ', Mã giảm giá: ' + paramOrderData.idVourcher}.\n` +
            `Phải thanh toán: ${paramOrderData.thanhTien - paramOrderData.thanhTien * gDiscount / 100} vnd${gDiscount ? ' (giảm giá ' + gDiscount + '%)' : ''}.`
        );
        if(!gDiscount) {
            // Nếu k tìm thấy voucher, reset voucher
            paramOrderData.idVourcher = '';
        }

        $('#preview-order-modal').modal();
    }

    // Hàm lấy phần trăm giảm giá
    function getOrderDiscount(paramVoucherId) {
        $.ajax({
            url: "http://203.171.20.210:8080/devcamp-pizza365/voucher_detail/" + paramVoucherId,
            async: false,
            type: "GET",
            dataType: 'json',
            success: handleVoucherFound,
            error: handleVoucherNotFound
        });
    }

    // Hàm khi tìm thấy voucher
    function handleVoucherFound(paramVoucherData) {
        gDiscount = paramVoucherData.phanTramGiamGia;
        $('#p-wrong-voucher').html('');
    }

    // Hàm khi không tìm thấy voucher
    function handleVoucherNotFound() {
        gDiscount = 0;
        $('#p-wrong-voucher').html('Mã giảm giá không tồn tại');
    }

    // Hàm tạo order
    function createOrder(paramObjRequest = gOrder) {
        $.ajax({
            url: "http://203.171.20.210:8080/devcamp-pizza365/orders",
            type: "POST",
            contentType: "application/json;charset=UTF-8",
            data: JSON.stringify(paramObjRequest),
            success: handleCreateOrderSuccess,
            error: handleError
        });
    }

    // Hàm khi tạo order thành công
    function handleCreateOrderSuccess(paramResponseData) {
        $('#inp-success-ma-don-hang').val(paramResponseData.orderCode);
        $('#order-success-modal').modal();
    }
});