$(document).ready(function () {
    /*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
    const gCOL_NAME = ['orderCode', 'kichCo', 'loaiPizza', 'idLoaiNuocUong', 'thanhTien', 'hoTen', 'soDienThoai', 'trangThai', 'action'];
    const gCOL_ORDER_ID = 0;
    const gCOL_KICH_CO_COMBO = 1;
    const gCOL_LOAI_PIZZA = 2;
    const gCOL_NUOC_UONG = 3;
    const gCOL_THANH_TIEN = 4;
    const gCOL_HO_VA_TEN = 5;
    const gCOL_SO_DIEN_THOAI = 6;
    const gCOL_TRANG_THAI = 7;
    const gCOL_ACTION = 8;
    var gId, gRowIndex;

    // Khai báo DataTable & mapping columns
    var gTable = $('#orders-table').DataTable({
        columns: [
            { data: gCOL_NAME[gCOL_ORDER_ID] },
            { data: gCOL_NAME[gCOL_KICH_CO_COMBO] },
            { data: gCOL_NAME[gCOL_LOAI_PIZZA] },
            { data: gCOL_NAME[gCOL_NUOC_UONG] },
            { data: gCOL_NAME[gCOL_THANH_TIEN] },
            { data: gCOL_NAME[gCOL_HO_VA_TEN] },
            { data: gCOL_NAME[gCOL_SO_DIEN_THOAI] },
            { data: gCOL_NAME[gCOL_TRANG_THAI] },
            { data: gCOL_NAME[gCOL_ACTION] },
        ],
        columnDefs: [
            {
                targets: gCOL_ACTION,
                defaultContent: '<button class="btn btn-warning btn-detail">Chi tiết</button>'
            }
        ],
        responsive: true
    });

    /*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */
    onPageLoading();
    // Gán sự kiện click cho nút chi tiết
    $('#orders-table').on('click', '.btn-detail', onBtnDetailClick);
    // Gán sự kiện click cho nút confirm
    $('#btn-confirm').on('click', onBtnConfirmClick);
    // Gán sự kiện click cho nút cancel
    $('#btn-cancel').on('click', onBtnCancelClick);

    /*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
    // Hàm khi load trang
    function onPageLoading() {
        getAllOrder();
        loadDrinkToSelect();
    }

    // Hàm khi nhấn nút chi tiết
    function onBtnDetailClick() {
        var vRow = gTable.row($(this).parents('tr'));
        var vRowData = vRow.data();
        gRowIndex = vRow.index();
        gId = vRowData.id;
        loadDataToModalForm(vRowData);
        $('#order-detail-modal').modal();
    }

    // Hàm khi bấm nút confirm
    function onBtnConfirmClick() {
        var vTrangThai = {
            trangThai: "confirmed"
        }
        handleUpdateTrangThaiOrder(vTrangThai);
    }

    // Hàm khi bấm nút confirm
    function onBtnCancelClick() {
        var vTrangThai = {
            trangThai: "cancel"
        }
        handleUpdateTrangThaiOrder(vTrangThai);
    }

    /*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
    // Hàm lấy tất cả order
    function getAllOrder() {
        $.ajax({
            url: 'http://203.171.20.210:8080/devcamp-pizza365/orders',
            type: "GET",
            dataType: 'json',
            success: handleGetAllOrderSuccess,
            error: handleError
        })
    }

    // Hàm khi lấy tất cả order thành công
    function handleGetAllOrderSuccess(paramResponseData) {
        loadOrderDataToTable(paramResponseData);
    }

    // Hàm khi gọi api thất bại
    function handleError(paramError) {
        console.assert(paramError.responseText);
    }

    // Hàm hiển thị data vào table
    function loadOrderDataToTable(paramOrderList) {
        gTable.clear();
        gTable.rows.add(paramOrderList);
        gTable.draw();
    }

    // Hàm load danh sách nước uống vào select
    function loadDrinkToSelect() {
        $.ajax({
            url: "http://203.171.20.210:8080/devcamp-pizza365/drinks",
            type: "GET",
            dataType: 'json',
            success: handleGetDrinkListSuccess,
            error: handleError
        });
    }

    // Hàm khi lấy danh sách đồ uống thành công
    function handleGetDrinkListSuccess(paramResponseData) {
        paramResponseData.forEach(bDrink => {
            $('#select-nuoc-uong').append($('<option>', {
                value: bDrink.maNuocUong,
                text: bDrink.tenNuocUong
            }));
        })
    }

    // Hàm hiển thị thông tin đơn hàng vào modal
    function loadDataToModalForm(paramOrderData) {
        $('#inp-order-code').val(paramOrderData.orderCode);
        $('#select-combo').val(paramOrderData.kichCo.toUpperCase());
        $('#inp-duong-kinh').val(paramOrderData.duongKinh);
        $('#inp-suon-nuong').val(paramOrderData.suon);
        $('#select-nuoc-uong').val(paramOrderData.idLoaiNuocUong);
        $('#inp-so-luong-nuoc').val(paramOrderData.soLuongNuoc);
        $('#inp-voucher-id').val(paramOrderData.idVourcher);
        $('#inp-loai-pizza').val(paramOrderData.loaiPizza);
        $('#inp-salad').val(paramOrderData.salad);
        $('#inp-thanh-tien').val(paramOrderData.thanhTien);
        $('#inp-giam-gia').val(paramOrderData.giamGia);
        $('#inp-ho-ten').val(paramOrderData.hoTen);
        $('#inp-email').val(paramOrderData.email);
        $('#inp-so-dien-thoai').val(paramOrderData.soDienThoai);
        $('#inp-dia-chi').val(paramOrderData.diaChi);
        $('#inp-loi-nhan').val(paramOrderData.loiNhan);
        $('#inp-trang-thai').val(paramOrderData.trangThai);
        $('#inp-ngay-tao').val(paramOrderData.ngayTao);
        $('#inp-ngay-cap-nhat').val(paramOrderData.ngayCapNhat);
    }

    // Hàm update trạng thái đơn hàng
    function handleUpdateTrangThaiOrder(paramTrangThai, paramId = gId) {
        $.ajax({
            url: "http://203.171.20.210:8080/devcamp-pizza365/orders/" + paramId,
            type: "PUT",
            contentType: "application/json;charset=UTF-8",
            data: JSON.stringify(paramTrangThai),
            success: handleUpdateTrangThaiOrderSuccess,
            error: handleError
        });
    }

    // Hàm khi update trạng thái đơn hàng thành công
    function handleUpdateTrangThaiOrderSuccess(paramResponseData) {
        alert('Cập nhật đơn hàng thành công');
        // Cập nhật lại row order
        var vRow = gTable.row(gRowIndex);
        var vRowData = vRow.data();
        vRowData[gCOL_NAME[gCOL_TRANG_THAI]] = paramResponseData.trangThai;
        vRow.data(vRowData);
        $('#order-detail-modal').modal('hide');
    }
});